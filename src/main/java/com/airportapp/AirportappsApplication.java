package com.airportapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 
 * @author niles the application where spring boot container will initialize....
 */
@SpringBootApplication
// to define the root package from where scanning should happen 
@ComponentScan(basePackages = "com")
// i am using @Scheduled will work only if i enable @EnableScheduling (fixedDelay)
@EnableScheduling
public class AirportappsApplication {
	public static void main(String[] args) {
		// Wrapping the application in the spring container....
		SpringApplication.run(AirportappsApplication.class, args);
	}

}
